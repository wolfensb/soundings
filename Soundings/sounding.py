# -*- coding: utf-8 -*-
"""
Created on Tue Feb 28 09:27:39 2017
@author: wolfensb
"""
import subprocess
import datetime
from numpy import genfromtxt
import os
import shutil
import matplotlib.lines as mlines
from . import SkewT
import matplotlib.pyplot as plt
            
# GLOBAL VALUES
DIR = os.path.dirname(os.path.realpath(__file__))
BASECOMMAND_NEW = 'http://weather.uwyo.edu/cgi-bin/bufrraob.py?datetime='
BASECOMMAND_OLD = 'http://weather.uwyo.edu/cgi-bin/sounding?region='
LIST_REGIONS = ['europe','south america','north america',
                'south pacific','antarctica','arctic','africa','mideast']

SOUNDING_DATA = genfromtxt(DIR+'/list_soundings.txt', delimiter=';',
                     dtype = ("|S30",float,float,int,"|S30",int,int,int))
LIST_REGIONS = ['europe','south america','north america',
                'south pacific','antarctica','arctic','africa','mideast']
START_BUFR = datetime.datetime(year = 2018, month = 5, day = 1)

all_STNM = [s[0][-5:].decode('utf-8') for s in SOUNDING_DATA]
all_NAMES =  [s[4] for s in SOUNDING_DATA]

def make_plot(date = None, station_no = '06610',  parcel = None, 
              plot_name = None, sounding_name = None, region='europe'):
    """
        Retrieves and plots a sounding for a given station and date
        
        Parameters
        ----------
        date : str (optional)
            Date in YYYY/MM/DD HH format, if not set current date will be used
        station_no : str (optional)
            Station number, see http://weather.uwyo.edu/upperair/sounding.html
            to get a list, default is Payerne
        region : str (optional)
            Region of the sounding, either europe,south america,north america,
            south pacific,antarctica,arctic,africa,mideast
            Only required prior to 1 May 2018
        parcel : str( optional)
            air parcel to consider, see https://pypi.python.org/pypi/SkewT
        plot_name : str (optional)
            filename of the sounding plot to generate
        sounding_name: str ( optional)
            filename of the sounding data
    """  
    # Assign specific defaults
    if not date:
        time = datetime.datetime.today()
    else:
        time = datetime.datetime.strptime(date,'%Y/%m/%d %H')
    resolution = datetime.timedelta(hours=12)
    time -= datetime.timedelta(hours=time.hour%(resolution.seconds/3600))

    if not plot_name:
        plot_name = './sounding_'+datetime.datetime.strftime(time,'%Y_%m_%d_%HZ') + '.pdf'
        
    
    
    try:
        idx_STNM = all_STNM.index(station_no)
        name_ST = all_NAMES[idx_STNM]
    except:
        raise ValueError('Specified station does not exist...')
        
    # Create command
    
    if os.path.exists('/tmp/sounding_filt.txt'):
        os.remove('/tmp/sounding_filt.txt')
    if os.path.exists('/tmp/sounding.txt'):
        os.remove('/tmp/sounding.txt')
    
    if time < START_BUFR:
        url = BASECOMMAND_OLD + region+'&TYPE=TEXT%3ALIST&YEAR='+str(time.year)
        url += '&MONTH='+str(time.month).zfill(2)+'&FROM='+str(time.day).zfill(2)+str(time.hour).zfill(2)
        url += '&TO='+str(time.day).zfill(2)+str(time.hour).zfill(2)
        url += '&STNM='+station_no
        
        
        cmd = 'w3m '+ '"'+url+'"'+ '>> /tmp/sounding.txt'
    
        subprocess.call(cmd,shell=True)
        subprocess.call('head --lines=-11 /tmp/sounding.txt > /tmp/sounding.txt',shell=True)
        if sounding_name:
            shutil.copyfile('/tmp/sounding.txt',sounding_name)
            
        valid = False
        if os.path.exists('/tmp/sounding_filt.txt'):
            if os.stat('/tmp/sounding.txt').st_size != 0:
                valid = True
    else:
        url = BASECOMMAND_NEW + datetime.datetime.strftime(time, '%Y-%m-%d') + '%20' + str(time.hour)
        url += ':00:00&id=' + station_no + '&type=TEXT:LIST'   
        
        cmd = 'w3m '+ '"'+url+'"'+ '>> /tmp/sounding.txt'
    
        subprocess.call(cmd,shell=True)
        subprocess.call("sed -i '/Observation/,$!d' /tmp/sounding.txt",shell=True)
        if sounding_name:
            shutil.copyfile('/tmp/sounding.txt',sounding_name)
            
        valid = False
        if os.path.exists('/tmp/sounding.txt'):
            if os.stat('/tmp/sounding.txt').st_size != 0:
                valid = True
                 
    if valid:
        plt.close('all')
        
       
        sounding= SkewT.Sounding('/tmp/sounding.txt')
        sounding.make_skewt_axes()
        sounding.add_profile(color='r',lw=2)
        
        blue_line = mlines.Line2D([], [], color='blue',linewidth=2,markersize=15, label='Dew point')
        red_line = mlines.Line2D([], [], color='red',linewidth=2, markersize=15, label='Prof. temp.')
        green_line = mlines.Line2D([], [], color='g',linewidth=2, linestyle='--', markersize=15, label='Isohumes (g/kg)')
        black_line = mlines.Line2D([], [], color='k', linewidth=2,linestyle='--', markersize=15, label='Dry adiabates') 
        mag_line = mlines.Line2D([], [], color='m', linewidth=2,linestyle='--', markersize=15, label='Wet adiabates')     
        handles = [blue_line,red_line,green_line,black_line,mag_line]
        labels = [h.get_label() for h in handles] 
        
        sounding.skewxaxis.set_title(name_ST+': '+sounding.skewxaxis.get_title(),fontsize=14)
        
        if type(parcel) == str:
            parcel = sounding.get_parcel(parcel)
        
        if parcel:
            sounding.lift_parcel(*parcel)  
        
        plt.legend(handles=handles, labels=labels,bbox_to_anchor=(3,0),loc='lower right')
        plt.savefig(plot_name,dpi=200)
        os.remove('/tmp/sounding.txt')
    else:
        print('Failed to download the data..., please check your inputs')
        print('You can also check if the following url exists: ')
        print(url)
