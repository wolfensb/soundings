#!/usr/bin/env python3
from Soundings import sounding
from optparse import OptionParser

def main():
    parser = OptionParser()
    parser.add_option("-d", "--date", dest="date",default=None,
                      help="Date in the YYYY/MM/DD HH format", metavar="DATE")
    parser.add_option("-s", "--station_no", dest="station_no",default='06610',
                      help="IGRA Station number, see https://www1.ncdc.noaa.gov/pub/data/igra/igra2-station-list.txt", metavar="STATION_NO")
    parser.add_option("-c", "--parcel", dest="parcel",default=None,
                      help="Parcel, see https://pypi.python.org/pypi/SkewT", metavar="PARCEL")
    parser.add_option("-p", "--plot_name",
                      dest="plot_name", default=None,
                      help="Name of the output radiosounding plot")                  
    parser.add_option("-o", "--sounding_name",
                      dest="sounding_name", default=None,
                      help="Name of the sounding file (if you want to save it)")   
    parser.add_option("-r", "--region",
                  dest="region", default='europe',
                  help="Only required before 1 May 2018! Region name, either: europe,south america,north america,south pacific,antarctica,arctic,africa,mideast")


    (options, args) = parser.parse_args()
    
    if options.parcel and options.parcel not in ['sb','mu','ml']:
        try:
            options.parcel = options.parcel.split(',')
            options.parcel[0] = float(options.parcel[0])
            options.parcel[1] = float(options.parcel[1])
            options.parcel[2] = float(options.parcel[2])
        except:
            print('Invalid parcel specification')
            options.parcel = None
        
    sounding.make_plot(date =  options.date,
    			station_no = options.station_no,
    			region = options.region,
                   		parcel = options.parcel,
    			plot_name = options.plot_name,
    			sounding_name = options.sounding_name)
                                       
