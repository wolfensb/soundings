from setuptools import setup

setup(name='Soundings',
      version='0.1',
      description='Download and plot radiosounding data',
      author='Daniel Wolfensberger - LTE EPFL',
      author_email='daniel.wolfensberger@epfl.ch',
      license='MIT',
      install_requires=[
         'numpy'
          ],
      package_data={'': ['list_soundings.txt']},
      include_package_data=True,
      packages=['Soundings'],
      entry_points = {
            'console_scripts':['plot_sounding = Soundings.plot_sounding:main']},
      zip_safe=False)

